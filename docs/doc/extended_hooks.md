# Extended Hooks

SuperBLT provides additional hook functionality by introducing a few new functions.
These work similarily to the regular BLT hook functions in terms of function arguments.

## Overriding a function

```lua
function Hooks:OverrideFunction(object, func, override)
```

Overrides a function completely while keeping existing hooks to it intact.

When pre/post hooks can't be used and a function has to be redefined completely,
using `OverrideFunction` will make sure any hooks that might have been added to it previously will stay intact.

## Retrieving a function

```lua
function Hooks:GetFunction(object, func)
```

Returns the current original function of an object, that is either the raw function if it hasn't been hooked
or the original function without any hooks attached to it.

Can be useful to call functions without invoking hooks or in combination with `OverrideFunction`.

## Retrieving the return value inside a hook

```lua
function Hooks:GetReturn()
```

Returns the return value(s) of the currently hooked function which are any values returned by
hook functions that were executed before the active hook function.

This allows access to the function return value inside a hook without having to do a function override.
