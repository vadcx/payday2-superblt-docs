# Automatic updates

In vanilla BLT, without a large amount of effort on your part, it's not possible to use
BLT's automatic updates system if your mod is not hosted on paydaymods.com, especially
when the mod is disabled.

SuperBLT adds a way to make your mods update themselves using customizable endpoints, fixing
this issue.

In your mod.txt file, the update tag now takes custom URLs:
```
"updates" : [
	{
		"identifier" : "anythingyouwant",
		"host": {
			"meta": "https://example.com/my_meta.json",
			"download": "https://example.com/download.zip",
			"patchnotes": "https://example.com/patchnotes.html"
		}
	}
]
```

The identifier can be anything you like - issues will arise if multiple mods have the same
identifier, so set it to something that you can reasonably assume no other mod will use.

The `meta` file should be in the following format:
```
[
	{
		"ident": "anythingyouwant",
		"version": "1.0",
		"patchnotes_url": "https://example.com/other-patchnotes.html",
		"download_url": "https://example.com/other-download.zip"
	}
]
```

The identifier in the `meta` must be the same as that in the update tag in your `mod.txt`.
The version tells SuperBLT what the newest version of your mod and will be compared to the
version in the `mod.txt` to determine if there is an update.

The `patchnotes_url` and `download_url` values are optional. If they are supplied, they override
the `pathnotes` and `download` values supplied in `mod.txt`. In fact, you can actually omit the
`download` and `patchnotes` values from `mod.txt` and only supply them in the update meta - that way,
if you don't have some kind of redirect system set up, you can have users update with a new archive file
for each update (do note that you'll have trouble if you don't supply them in either).

Instead of providing a version it is also possible to provide a hash value of the mod.
For this you would simply replace the version entry in the `meta` file with the current hash:
```
[
	{
		"ident": "anythingyouwant",
		"hash": "43e7cd36567c755be88e60bde45ba418527c692af982d45fbedb8b5a8c792772"
	}
]
```

The hash is a SHA-256 hash that is built by first creating the hashes of all individual files
and then concatenating all of these file hashes in alphabetical (based on their original names) order
and creating the final hash from that.

